#!/bin/sh

echo ///////////////////////////////////////////////////////////////////////////////
echo //                                                                           //
echo //                        flash NOKIA 3.2 A + B Slot                         // 
echo //                                by Hacktor                                 //
echo //                                    2022                                   //
echo //                                                                           //
echo ///////////////////////////////////////////////////////////////////////////////

####flashen

#org boot.img
fastboot flash boot boot.img

#root boot.img
#fastboot flash boot magisk_patched-23000_6XkRZ.img

fastboot flash custom custom.img

fastboot flash system system.img

fastboot flash vbmeta vbmeta.img

fastboot flash vendor vendor.img

fastboot flash keymaster keymaster.img

fastboot flash cmnlib64 cmnlib64.img

fastboot flash cmnlib cmnlib.img

fastboot flash mdtp mdtp.img

fastboot flash dtbo dtbo.img

fastboot flash aboot aboot.img

fastboot flash picture picture.img

fastboot flash splash splash.img

fastboot flash dsp dsp.img

fastboot flash devcfg devcfg.img

fastboot flash tz tz.img

fastboot flash rpm rpm.img

fastboot flash sbl1 sbl1.img

fastboot flash modem modem.img

# userdata löschen
sleep 55s
fastboot -w

# slot b aktiv setzen
#fastboot --set-active=b

#inaktiven slot aktiv setzen
sleep 33s
fastboot set_active other

#reboot to fastboot
sleep 22s
fastboot reboot bootloader

sleep 55s

####flashen

#org boot.img
fastboot flash boot boot.img

#root boot.img
#fastboot flash boot magisk_patched-23000_6XkRZ.img

fastboot flash custom custom.img

fastboot flash system system.img

fastboot flash vbmeta vbmeta.img

fastboot flash vendor vendor.img

fastboot flash keymaster keymaster.img

fastboot flash cmnlib64 cmnlib64.img

fastboot flash cmnlib cmnlib.img

fastboot flash mdtp mdtp.img

fastboot flash dtbo dtbo.img

fastboot flash aboot aboot.img

fastboot flash picture picture.img

fastboot flash splash splash.img

fastboot flash dsp dsp.img

fastboot flash devcfg devcfg.img

fastboot flash tz tz.img

fastboot flash rpm rpm.img

fastboot flash sbl1 sbl1.img

fastboot flash modem modem.img

# userdata löschen
sleep 55s
fastboot -w

#fastboot reboot

